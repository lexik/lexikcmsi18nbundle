<?php

namespace Lexik\Bundle\CMSI18nBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Lexik\Bundle\CMSI18nBundle\Entity\BlockTranslation;

/**
 * Class BlockTranslationRepository.
 */
class BlockTranslationRepository extends EntityRepository
{
    /**
     * @param $reference
     * @param $locale
     * @param $defaultLocale
     *
     * @return BlockTranslation|null
     */
    public function findOneForLocaleAndReference($reference, $locale, $defaultLocale)
    {
        if (($page = $this->findByLocaleAndReference($locale, $reference))) {
            return $page;
        }

        if (($page = $this->findByLocaleAndReference($defaultLocale, $reference))) {
            return $page;
        }

        return null;
    }

    /**
     * @param $locale
     * @param $reference
     *
     * @return BlockTranslation|null
     */
    public function findByLocaleAndReference($locale, $reference)
    {
        $result = $this->createQueryBuilder('t')
            ->innerJoin('t.block', 'b', 'WITH', 'b.reference = :reference')
            ->where('t.locale = :locale')
            ->setParameters([
                'reference' => $reference,
                'locale'    => $locale,
            ])
            ->getQuery()
            ->getResult();

        return count($result) ? $result[0] : null;
    }
}
