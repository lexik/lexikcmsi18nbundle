<?php

namespace Lexik\Bundle\CMSI18nBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository for Block entity.
 */
class BlockRepository extends EntityRepository
{
    /**
     * Returns cached blocks.
     *
     * @param bool $cache
     *
     * @return array
     */
    public function getCachedBlocks($cache = true)
    {
        $query = $this->createQueryBuilder('b')
            ->select('b, t')
            ->leftJoin('b.translations', 't')
            ->getQuery();

        if ($cache) {
            $query->useResultCache(true, 3600 * 24 * 30, 'blocks');
        }

        return $query->getArrayResult();
    }
}
