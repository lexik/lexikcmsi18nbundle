<?php

namespace Lexik\Bundle\CMSI18nBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Lexik\Bundle\CMSI18nBundle\Entity\PageTranslation;

/**
 * Class PageTranslationRepository.
 */
class PageTranslationRepository extends EntityRepository
{
    /**
     * @param string $reference
     * @param string $locale
     *
     * @return PageTranslation|null
     */
    public function findOneForLocaleAndReference($reference, $locale, $defaultLocale)
    {
        if (($page = $this->findByLocaleAndReference($locale, $reference))) {
            return $page;
        }

        if (($page = $this->findByLocaleAndReference($defaultLocale, $reference))) {
            return $page;
        }

        return null;
    }

    /**
     * @param string $locale
     * @param string $reference
     *
     * @return PageTranslation|null
     */
    public function findByLocaleAndReference($locale, $reference)
    {
        $result = $this->createQueryBuilder('t')
            ->innerJoin('t.page', 'p', 'WITH', 'p.reference = :reference')
            ->where('t.locale = :locale')
            ->setParameters([
                'reference' => $reference,
                'locale'    => $locale,
            ])
            ->getQuery()
            ->getResult();

        return count($result) ? $result[0] : null;
    }

    /**
     * @param string $slug
     * @param string $locale
     *
     * @return PageTranslation|null
     */
    public function findOneForLocaleAndSlug($slug, $locale, $defaultLocale)
    {
        if (($page = $this->findByLocaleAndSlug($locale, $slug))) {
            return $page;
        }

        if (($page = $this->findByLocaleAndSlug($defaultLocale, $slug))) {
            return $page;
        }

        return null;
    }

    /**
     * @param $locale
     * @param $slug
     *
     * @return PageTranslation|null
     */
    public function findByLocaleAndSlug($locale, $slug)
    {
        $result = $this->createQueryBuilder('t')
            ->where('t.locale = :locale')
            ->andWhere('t.slug = :slug')
            ->setParameters([
                'locale' => $locale,
                'slug'   => $slug,
            ])
            ->getQuery()
            ->getResult();

        return count($result) ? $result[0] : null;
    }
}
