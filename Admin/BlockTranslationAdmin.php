<?php

namespace Lexik\Bundle\CMSI18nBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;

/**
 * BlockTranslationAdmin.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 */
class BlockTranslationAdmin extends BaseTranslationAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'locale',
                'hidden',
                [
                    'required' => true,
                ]
            )
            ->add(
                'content',
                'textarea',
                [
                    'required' => true,
                    'attr'     => [
                        'rows'        => 20,
                        'ckeditor'    => true,
                        'data-editor' => 'html',
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getParentAssociationMapping()
    {
        return 'block';
    }
}
