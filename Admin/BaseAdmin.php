<?php

namespace Lexik\Bundle\CMSI18nBundle\Admin;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * BaseAdmin.
 *
 * @author Nicolas Cabot <n.cabot@lexik.fr>
 */
class BaseAdmin extends Admin
{
    /**
     * @var string
     */
    protected $translationCreateRoute;

    /**
     * @var string
     */
    protected $translationEditRoute;

    /**
     * @var array
     */
    protected $preferredLocales;

    /**
     * @param array $locales
     */
    public function setPreferredLocales($locales)
    {
        $this->preferredLocales = $locales;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('reference')
            ->add('description')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('reference')
            ->add('description')
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'show'   => [],
                        'edit'   => [],
                        'delete' => [],
                    ],
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('reference');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
                ->add('reference', 'text', ['required' => true])
                ->add('description', 'textarea', ['required' => false])
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit'])) {
            return;
        }

        $id     = $this->getRequest()->get('id');
        $object = $this->getObject($id);

        foreach ($this->preferredLocales as $locale) {
            if (($translation = $object->hasTranslationForLocale($locale))) {
                $params = [
                    'route'           => $this->translationEditRoute,
                    'routeParameters' => ['id' => $id, 'childId' => $translation->getId()],
                    'routeAbsolute'   => false,
                ];
                $class = 'translation-existing';
            } else {
                $params = [
                    'route'           => $this->translationCreateRoute,
                    'routeParameters' => ['id' => $id, 'locale' => $locale],
                    'routeAbsolute'   => false,
                ];
                $class = 'translation-missing';
            }

            $menuItem = $menu->addChild(strtoupper($locale), $params);
            $menuItem->setLinkAttribute('class', $class);
        }
    }
}
