<?php

namespace Lexik\Bundle\CMSI18nBundle\Admin;

use Lexik\Bundle\CMSI18nBundle\Service\PageTransformer;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * PageTranslationAdmin.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 */
class PageTranslationAdmin extends BaseTranslationAdmin
{
    /**
     * @var PageTransformer
     */
    protected $pageTransformer;

    /**
     * @param PageTransformer $pageTransformer
     */
    public function setPageTransformer(PageTransformer $pageTransformer)
    {
        $this->pageTransformer = $pageTransformer;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'locale',
                'hidden',
                [
                    'required' => true,
                ]
            )
            ->add('slug')
            ->add('metaTitle')
            ->add(
                'metaDescription',
                null,
                [
                    'attr' => ['rows' => 20],
                ]
            )
            ->add(
                'content',
                null,
                [
                    'attr' => [
                        'rows'        => 20,
                        'ckeditor'    => true,
                        'data-editor' => 'html',
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getParentAssociationMapping()
    {
        return 'page';
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($object)
    {
        $this->pageTransformer->transform($object);
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($object)
    {
        $this->pageTransformer->transform($object);
    }
}
