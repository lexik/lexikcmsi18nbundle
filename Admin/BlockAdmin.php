<?php

namespace Lexik\Bundle\CMSI18nBundle\Admin;

/**
 * BlockAdmin.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 */
class BlockAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $translationCreateRoute = 'admin_lexik_cmsi18n_block_blocktranslation_create';

    /**
     * @var string
     */
    protected $translationEditRoute = 'admin_lexik_cmsi18n_block_blocktranslation_edit';
}
