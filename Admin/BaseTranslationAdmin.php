<?php

namespace Lexik\Bundle\CMSI18nBundle\Admin;

use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * BaseTranslationAdmin.
 *
 * @author Nicolas Cabot <n.cabot@lexik.fr>
 */
class BaseTranslationAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list');
    }

    /**
     * {@inheritdoc}
     */
    public function getNewInstance()
    {
        $object = parent::getNewInstance();
        $object->setLocale($this->request->get('locale'));

        return $object;
    }

    /**
     * @param string         $action
     * @param AdminInterface $childAdmin
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function getSideMenu($action, AdminInterface $childAdmin = null)
    {
        $menu = parent::getSideMenu($action, $childAdmin);

        if (!$this->getRequest()->query->has('childId')) {
            $locale = $this->getRequest()->get('locale');

            /** @var ItemInterface $child */
            foreach ($menu->getChildren() as $child) {
                if ($child->getName() === strtoupper($locale)) {
                    $child->setCurrent(true);
                }
            }
        }

        return $menu;
    }
}
