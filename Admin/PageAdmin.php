<?php

namespace Lexik\Bundle\CMSI18nBundle\Admin;

/**
 * PageAdmin.
 *
 * @author Laurent Heurtault <l.heurtault@lexik.fr>
 */
class PageAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $translationCreateRoute = 'admin_lexik_cmsi18n_page_pagetranslation_create';

    /**
     * @var string
     */
    protected $translationEditRoute = 'admin_lexik_cmsi18n_page_pagetranslation_edit';
}
