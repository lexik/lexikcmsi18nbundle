<?php

namespace Lexik\Bundle\CMSI18nBundle\Twig;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class CMSExtension.
 */
class CMSExtension extends \Twig_Extension
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var ManagerRegistry
     */
    private $registry;

    /**
     * @var string
     */
    private $currentLocale;

    /**
     * @var string
     */
    private $defaultLocale;

    /**
     * @var \Twig_Environment
     */
    private $environment;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->router        = $container->get('router');
        $this->registry      = $container->get('doctrine');
        $requestStack        = $container->get('request_stack');
        $this->currentLocale = $requestStack->getCurrentRequest()
            ? $requestStack->getCurrentRequest()->getLocale()
            : 'en';
        $this->defaultLocale = $container->getParameter('locale');
        $this->environment   = $container->get('twig');
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('page_path', [$this, 'getPagePath']),
            new \Twig_SimpleFunction('page_content', [$this, 'getPageContent'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('cms_block', [$this, 'getBlockContent'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param $reference
     *
     * @return string
     */
    public function getPagePath($reference)
    {
        $page = $this
            ->getEntityManager()
            ->getRepository('LexikCMSI18nBundle:PageTranslation')
            ->findOneForLocaleAndReference($reference, $this->currentLocale, $this->defaultLocale);

        if ($page) {
            return $this->router->generate('lexik_cms_page', [
                '_locale' => $this->currentLocale,
                'slug'    => $page->getSlug(),
            ]);
        }

        return '#';
    }

    /**
     * @param $reference
     * @param null $object
     *
     * @return string
     */
    public function getBlockContent($reference, $object = null)
    {
        return $this->environment->render('LexikCMSI18nBundle:Default:block.html.twig', [
            'templateName' => $reference,
            'object'       => $object,
        ]);
    }

    /**
     * @param string $slug
     *
     * @return string
     */
    public function getPageContent($slug)
    {
        $page = $this
            ->getEntityManager()
            ->getRepository('LexikCMSI18nBundle:PageTranslation')
            ->findOneForLocaleAndSlug($slug, $this->currentLocale, $this->defaultLocale);

        if ($page) {
            return $page->getTransformedContent();
        }

        return '';
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager
     */
    protected function getEntityManager()
    {
        return $this->registry->getManager();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lexik_cms_extension';
    }
}
