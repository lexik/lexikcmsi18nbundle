<?php

namespace Lexik\Bundle\CMSI18nBundle\Twig\Loader;

use Doctrine\Common\Persistence\ManagerRegistry;
use Lexik\Bundle\CMSI18nBundle\Entity\BlockTranslation;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class DatabaseTwigLoader.
 *
 * @author Gilles Gauthier <g.gauthier@lexik.fr>
 */
class DatabaseTwigLoader implements \Twig_LoaderInterface, \Twig_ExistsLoaderInterface
{
    /**
     * @param ManagerRegistry $registry
     */
    private $registry;

    /**
     * @var string
     */
    private $currentLocale;

    /**
     * @var string
     */
    private $defaultLocale;

    /**
     * @param ManagerRegistry $registry
     * @param RequestStack    $requestStack
     * @param $locale
     */
    public function __construct(ManagerRegistry $registry, RequestStack $requestStack, $locale)
    {
        $this->registry      = $registry;
        $this->currentLocale = $requestStack->getCurrentRequest()
            ? $requestStack->getCurrentRequest()->getLocale()
            : $locale;
        $this->defaultLocale = $locale;
    }

    /**
     * {@inheritdoc}
     */
    public function getSource($reference)
    {
        if (false === $source = $this->getValue($reference)) {
            throw new \Twig_Error_Loader(sprintf('Template "%s" does not exist.', $reference));
        }

        return $source->getContent();
    }

    /**
     * {@inheritdoc}
     */
    public function exists($name)
    {
        return $this->getValue($name) !== null;
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheKey($name)
    {
        return sprintf('%s_%s', $name, $this->currentLocale);
    }

    /**
     * {@inheritdoc}
     */
    public function isFresh($slug, $time)
    {
        $source = $this->getValue($slug);
        if (false === $lastModified = $source->getUpdatedAt()->getTimestamp()) {
            return false;
        }

        return $lastModified <= $time;
    }

    /**
     * @param string $reference
     *
     * @return BlockTranslation|null
     */
    protected function getValue($reference)
    {
        $blockTranslation = $this
            ->registry->getManager()
            ->getRepository('LexikCMSI18nBundle:BlockTranslation')
            ->findOneForLocaleAndReference($reference, $this->currentLocale, $this->defaultLocale);

        if ($blockTranslation) {
            return $blockTranslation;
        }

        return null;
    }
}
