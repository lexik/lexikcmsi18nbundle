<?php

namespace Lexik\Bundle\CMSI18nBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CMSController.
 */
class CMSController extends Controller
{
    /**
     * @param string $_locale
     * @param string $slug
     *
     * @return Response
     */
    public function pageAction($_locale, $slug)
    {
        $page = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository('LexikCMSI18nBundle:PageTranslation')
            ->findOneForLocaleAndSlug($slug, $_locale, $this->get('service_container')->getParameter('locale'));

        if (!$page) {
            throw $this->createNotFoundException();
        }

        return $this->render('LexikCMSI18nBundle:Default:page.html.twig', [
            'page'            => $page,
            'frontend_layout' => $this->container->getParameter('lexik_cms.frontend_layout'),
        ]);
    }
}
