Getting started
===============

Prerequisites
-------------

This bundle requires Symfony 2.8+

Installation
------------

Add [`composer require lexik/cmsi18n-bundle`](https://packagist.org/packages/lexik/cmsi18n-bundle)
to your `composer.json` file:

``` bash
php composer.phar require "lexik/cmsi18n-bundle"
```
    

Register the bundle in `app/AppKernel.php`:

``` php
public function registerBundles()
{
    return array(
        // ...
        new Lexik\Bundle\CMSI18nBundle\LexikCMSI18nBundle(),
    );
}
```

Configuration
-------------

Configure the SSH keys path in your `config.yml` :

``` yaml
lexik_cmsi18n:
    frontend_layout: '::base.html.twig'
    preferred_locales: []
    templating_extensions: []
```

Usage
-------------

# Twig

``` twig
{{ page_path(block_reference) }}

{{ page_content(block_reference) }}

{{ cms_block(block_reference) }}
```
