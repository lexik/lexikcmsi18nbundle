<?php

namespace Lexik\Bundle\CMSI18nBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('lexik_cmsi18n');

        $rootNode
            ->children()
                ->scalarNode('frontend_layout')
                    ->defaultValue('::frontend.html.twig')
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('preferred_locales')
                    ->defaultValue([])
                    ->prototype('scalar')
                    ->end()
                ->end()
                ->arrayNode('templating_extensions')
                    ->defaultValue([])
                    ->prototype('scalar')
                    ->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
