<?php

namespace Lexik\Bundle\CMSI18nBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * LexikCMSI18nExtension.
 */
class LexikCMSI18nExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');
        $loader->load('twig.xml');
        $loader->load('admins.xml');

        foreach ($config as $name => $value) {
            $container->setParameter(sprintf('lexik_cms.%s', $name), $value);
        }

        $templating = $container->findDefinition('lexik_cms.templating');

        foreach ($config['templating_extensions'] as $extensionsId) {
            $templating->addMethodCall('addExtension', [new Reference($extensionsId)]);
        }
    }
}
