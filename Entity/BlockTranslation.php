<?php

namespace Lexik\Bundle\CMSI18nBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="lexik_cms_block_translation")
 * @ORM\Entity(repositoryClass="Lexik\Bundle\CMSI18nBundle\Repository\BlockTranslationRepository")
 */
class BlockTranslation
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string")
     *
     * @Assert\NotBlank()
     */
    private $locale;

    /**
     * @var Block
     *
     * @ORM\ManyToOne(targetEntity="Block", inversedBy="translations")
     * @ORM\JoinColumn(name="block_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $block;

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s_%s', $this->block->getReference(), $this->locale);
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content.
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return Block
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @param Block $block
     */
    public function setBlock($block)
    {
        $this->block = $block;
    }
}
