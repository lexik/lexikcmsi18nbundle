<?php

namespace Lexik\Bundle\CMSI18nBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="lexik_cms_page")
 * @ORM\Entity(repositoryClass="Lexik\Bundle\CMSI18nBundle\Repository\PageRepository")
 *
 * @UniqueEntity({"reference"})
 */
class Page
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", unique=true)
     *
     * @Assert\NotBlank()
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PageTranslation", mappedBy="page", cascade={"all"})
     */
    private $translations;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * Returns the title.
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->reference;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param PageTranslation $translation
     */
    public function addTranslation(PageTranslation $translation)
    {
        $this->translations[] = $translation;
        $translation->setPage($this);
    }

    /**
     * @param Collection $translations
     */
    public function setTranslations(Collection $translations)
    {
        foreach ($translations as $translation) {
            $translation->setPage($this);
        }

        $this->translations = $translations;
    }

    /**
     * @return ArrayCollection|PageTranslation[]
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param $locale
     *
     * @return PageTranslation|false
     */
    public function hasTranslationForLocale($locale)
    {
        foreach ($this->getTranslations() as $translation) {
            if ($translation->getLocale() == $locale) {
                return $translation;
            }
        }

        return false;
    }
}
