<?php

namespace Lexik\Bundle\CMSI18nBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="lexik_cms_block")
 * @ORM\Entity(repositoryClass="Lexik\Bundle\CMSI18nBundle\Repository\BlockRepository")
 *
 * @UniqueEntity({"reference"})
 */
class Block
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", unique=true)
     *
     * @Assert\NotBlank()
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="BlockTranslation", mappedBy="block", cascade={"all"})
     */
    private $translations;

    /**
     * The locale to use to get the right block content.
     *
     * @var string
     */
    private $locale;

    /**
     * @var BlockTranslation
     */
    private $currentTranslation;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->reference;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param BlockTranslation $translation
     */
    public function addTranslation(BlockTranslation $translation)
    {
        $this->translations[] = $translation;
        $translation->setBlock($this);
    }

    /**
     * @param Collection $translations
     */
    public function setTranslations(Collection $translations)
    {
        foreach ($translations as $translation) {
            $translation->setBlock($this);
        }

        $this->translations = $translations;
    }

    /**
     * @return ArrayCollection|BlockTranslation[]
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param string $locale
     *
     * @return PageTranslation|false
     */
    public function hasTranslationForLocale($locale)
    {
        foreach ($this->getTranslations() as $translation) {
            if ($translation->getLocale() == $locale) {
                return $translation;
            }
        }

        return false;
    }

    /**
     * @throws \Exception
     */
    protected function setCurrentTranslation()
    {
        if (false !== $translation = $this->hasTranslationForLocale($this->locale)) {
            $this->currentTranslation = $translation;
        } else {
            throw new \Exception(sprintf('No translation found for locale %s', $this->locale));
        }
    }

    /**
     * @param $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        $this->setCurrentTranslation();
    }

    /**
     * @return string
     */
    public function getChecksum()
    {
        return md5(sprintf('%s_%s', $this->locale, $this->getReference()));
    }

    /**
     * @return string
     */
    public function getLastModifiedTimestamp()
    {
        $date = $this->currentTranslation->getUpdatedAt();

        if (!$date instanceof \DateTime) {
            $date = new \DateTime('now');
        }

        return $date->format('U');
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->currentTranslation->getContent();
    }
}
