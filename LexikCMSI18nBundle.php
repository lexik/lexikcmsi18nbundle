<?php

namespace Lexik\Bundle\CMSI18nBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * LexikCMSI18nBundle.
 */
class LexikCMSI18nBundle extends Bundle
{
}
