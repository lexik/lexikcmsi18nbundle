<?php

namespace Lexik\Bundle\CMSI18nBundle\Service;

use Lexik\Bundle\CMSI18nBundle\Entity\PageTranslation;
use Lexik\Bundle\CMSI18nBundle\Repository\PageTranslationRepository;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class PageTransformer.
 */
class PageTransformer
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var PageTranslationRepository
     */
    protected $repository;

    /**
     * @param RouterInterface           $router
     * @param PageTranslationRepository $repository
     */
    public function __construct(RouterInterface $router, PageTranslationRepository $repository)
    {
        $this->router     = $router;
        $this->repository = $repository;
    }

    /**
     * @param PageTranslation $translation
     */
    public function transform(PageTranslation $translation)
    {
        $translation->setTransformedContent(
            $this->transformPageLinks($translation->getContent(), $translation->getLocale())
        );
    }

    /**
     * @param string $content
     * @param string $locale
     *
     * @return string
     */
    protected function transformPageLinks($content, $locale)
    {
        $router = $this->router;
        $repo   = $this->repository;

        $content = preg_replace_callback(
            '`#page:([^#]+)#`',
            function ($matches) use ($router, $repo, $locale) {
                $page = $repo->findByLocaleAndReference($locale, $matches[1]);

                return $page
                    ? $router->generate('lexik_cms_page', ['_locale' => $locale, 'slug' => $page->getSlug()])
                    : '#';
            },
            $content
        );

        return $content;
    }
}
